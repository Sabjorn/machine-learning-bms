from PIL import Image
import tensorflow as tf
import numpy as np
import math
from matplotlib import pyplot as plt
import json
import dvclive
from dvclive.keras import DvcLiveCallback
import argparse
import yaml
import tf2onnx

argParser = argparse.ArgumentParser()
argParser.add_argument(
    'x', help="path of x.npy, generated in the preparing stage")
argParser.add_argument(
    'y', help="path of y.npy, generated in the preparing stage")
argParser.add_argument(
    'paramsPath', help="path to params.yaml")
argParser.add_argument(
    'outPath', help="path to output trained ONNX model")

args = argParser.parse_args()

with open('params.yaml', 'r') as fd:
    params = yaml.safe_load(fd)


def visualize(cnnWeights, linearWeights):
    channels = cnnWeights.shape[3]
    columns = math.ceil(math.sqrt(channels))
    rows = math.ceil(channels / columns)

    figsize = plt.figaspect((25*rows)/(8*columns))*3

    fig, axes = plt.subplots(
        columns, rows, figsize=figsize)

    maxV = np.average(np.abs(np.ravel(cnnWeights)))*2

    linearWeights = np.ravel(linearWeights)
    iSorted = np.argsort(linearWeights)

    for i in range(channels):
        c = i % columns
        r = math.floor(i / columns)
        ax = axes[r, c]

        ax.set_title(f"{linearWeights[iSorted[i]]:#0.2}")
        ax.imshow(
            cnnWeights[:, :, 0, iSorted[i]],
            vmin=-maxV,
            vmax=maxV,
            interpolation='none'
        )
        ax.set(xticks=[], yticks=[])

    fig.subplots_adjust(
        left=0.03,
        right=1-0.03,
        top=1-0.03*8/25,
        bottom=0.03*8/25
    )

    fig.tight_layout()

    fig.canvas.draw()
    imarray = np.array(fig.canvas.renderer.buffer_rgba())

    img = Image.fromarray(imarray)

    return img


print("Initializing Tensorflow...")

def tanhexp(x):
    return x * tf.math.tanh(tf.math.exp(x))

model = tf.keras.models.Sequential([
    tf.keras.layers.InputLayer(input_shape=(25, 8)),
    tf.keras.layers.Reshape((25, 8, 1)),
    tf.keras.layers.ZeroPadding2D((12, 0)),
    tf.keras.layers.Conv2D(
        params['channels'],
        (25, 8),
        activation=tanhexp,
        kernel_regularizer=tf.keras.regularizers.L1L2(
            params['l1reg'],
            params['l2reg']
        )
    ),
    tf.keras.layers.AveragePooling2D((25, 1)),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(
        1,
        kernel_regularizer=tf.keras.regularizers.L1L2(
            params['l1reg'],
            params['l2reg']
        )
    )
])

model.compile(
    optimizer=tf.keras.optimizers.Adam(learning_rate=params['learningrate']),
    loss=params['lossfunc'],
    metrics=[tf.keras.metrics.MeanAbsoluteError()]
)

print("Loading dataset...")

X = np.array(json.load(open(args.x, 'r')))
Y = np.array(json.load(open(args.y, 'r')))

print("Training model...")

live_dvc = dvclive.Live()

model.fit(
    X,
    Y,
    epochs=params['epochs'],
    validation_split=params['validation_ratio'],
    batch_size=params['batchsize'],
    callbacks=[DvcLiveCallback()]
)

print("Saving trained model...")

tf2onnx.convert.from_keras(model, output_path=args.outPath)

cnnWeights, _ = model.layers[2].get_weights()
linearWeights, _ = model.layers[5].get_weights()

img = visualize(cnnWeights, linearWeights)
live_dvc.log_image('chart-features.png', img)

print("Done.")
