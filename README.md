# Machine Learning BMS

BMS を機械学習するぞオラ見たかてめえこの

## ディレクトリ構成

```
machine-learning-bms
├── data
│   ├── replays                     # 各々のリプレイファイル（各プレイヤーごとに ZIP でアーカイブ）
│   │   ├── replays-sabjorn.zip     # 名称は自由。ZIP 内のファイル構造も自由。
│   │   └── ...                    
│   └── bms.zip                     # リプレイと照らし合わせるための BMS 譜面をまとめた ZIP ファイル
├── bin
│   ├── generate-bms-dataset        # リプレイと BMS からデータセットを生成するバイナリ .NET 製
│   └── train.py                    # データセットから学習を行いモデルを生成するスクリプト
├── dvc.yaml                        # DVC のパイプライン定義ファイル
├── Dockerfile                      # 再現環境構築用 Dockerfile
├── repro-docker.bat                # Docker 上で DVC パイプラインを再現 (dvc repro) するバッチファイル
└── README.md
```
