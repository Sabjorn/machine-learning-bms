docker build -t machine-learning-bms -f dockerfiles/build-env/Dockerfile .

docker run -ti --rm -v %cd%:/workspace --gpus all --name ml-bms machine-learning-bms bash -c "dvc pull && dvc repro"